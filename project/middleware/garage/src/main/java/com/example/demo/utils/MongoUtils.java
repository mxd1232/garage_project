package com.example.demo.utils;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class MongoUtils {

    private static final String MONGO_DB_HOST_NAME = "localhost";
    private static final String MONGO_DATABASE_NAME = "test";
    private static final int MONGO_DB_PORT = 27017;

    private static final MongoClient client = new MongoClient(MONGO_DB_HOST_NAME, MONGO_DB_PORT);
    private static final MongoDatabase dataBase = client.getDatabase(MONGO_DATABASE_NAME);

}

