package com.example.demo.utils;

import com.example.demo.models.Person;
import com.example.demo.repositories.PersonRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

public class Security {
    private static String SECRET_KEY = "SECRET_KEY";

    public static String getSecretKey() {
        return SECRET_KEY;
    }

    public static void setPassword(String secretKey) {
        Security.SECRET_KEY = secretKey;
    }

    public String getToken(Person person, PersonRepository personRepository) {
        return generateToken(person, personRepository);
    }

    private Person getPersonByLogin(Person person, PersonRepository personRepository) {

        return personRepository.findAll().stream().filter(p -> p.getLogin().equals(person.getLogin())).findFirst().get();
    }

    private boolean isCorrectPassword(Person person, PersonRepository personRepository) {
        Person personWithCheckedLogin = getPersonByLogin(person, personRepository);

        return personWithCheckedLogin.getLogin().equals(person.getLogin()) && personWithCheckedLogin.getPassword().equals(person.getPassword());
    }

    private String generateToken(Person person, PersonRepository personRepository) {

        if (isCorrectPassword(person, personRepository)) {
            long currentTimeMillis = System.currentTimeMillis();

            return Jwts.builder()
                    .claim("login", person.getLogin())
                    .claim("password", person.getPassword())
                    .claim("roles", person.getRole())
                    .setIssuedAt(new Date(currentTimeMillis))
                    .setExpiration(new Date(currentTimeMillis + 250000))
                    .signWith(SignatureAlgorithm.HS512, Security.getSecretKey())
                    .compact();
        } else
            return "INCORRECT_DATES";

    }


}
