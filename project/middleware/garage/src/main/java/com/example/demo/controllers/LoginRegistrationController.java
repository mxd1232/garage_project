package com.example.demo.controllers;

import com.example.demo.models.Person;
import com.example.demo.repositories.PersonRepository;
import com.example.demo.utils.LoginRegistrationUtils;
import com.example.demo.utils.Security;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginRegistrationController {

    @Autowired
    private PersonRepository personRepository;

    private LoginRegistrationUtils loginRegistrationUtils = new LoginRegistrationUtils();
    private Security security = new Security();

    @PostMapping("/logIn")
    public String login(@RequestBody Person person) {
        return security.getToken(person,personRepository);
    }

    @PostMapping("/register")
    public String register(@RequestBody Person person) {
        return loginRegistrationUtils.register(person, personRepository);
    }
}
