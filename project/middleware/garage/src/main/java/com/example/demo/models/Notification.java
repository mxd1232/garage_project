package com.example.demo.models;

import com.example.demo.enums.KindNotification;
import com.example.demo.enums.StatusNotification;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Data
public class Notification {
    @Id
    private String id;
    private Person client;
    private Car clientCar;
    private LocalDate dateVisit;
    private String problemDescription;
    private KindNotification kindNotification;
    private StatusNotification statusNotification;

    public Notification(Person client, Car clientCar, LocalDate dateVisit, String problemDescription, KindNotification kindNotification) {
        this.client = client;
        this.clientCar = clientCar;
        this.dateVisit = dateVisit;
        this.problemDescription = problemDescription;
        this.kindNotification = kindNotification;
        this.statusNotification = StatusNotification.NEW;
    }
}
