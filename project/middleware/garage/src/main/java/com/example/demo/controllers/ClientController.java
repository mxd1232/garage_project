package com.example.demo.controllers;

import com.example.demo.enums.Role;
import com.example.demo.models.Notification;
import com.example.demo.models.Person;
import com.example.demo.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ClientController {

    @Autowired
    ClientService clientService;

    @PostMapping("/service/orderVisit")
    public void addNotification(@RequestBody Notification notification) {
//        Person person = new Person("Jarosław","Spyrka","607289890","J.Spyrka@dziewczyny.pl");
//        Car car = new Car("BMW","E46", LocalDate.of(2001,7,24), Engine.DIESEL,184);
//        Notification notification = new Notification(person,car,LocalDate.of(2020,6,22),"Nawiew przestał działać", KindNotification.REPAIR);
        clientService.savePerson(notification.getClient());
        clientService.saveNotification(notification);

    }

    @PostMapping("/service/editVisit")
    public void editNotification(@RequestBody Notification notification) {
        clientService.editNotification(notification);
    }

    @GetMapping("/test")
    public Person test() {

        Person person = new Person("user", "user1", Role.USER, "Kamil", "Sitek", "325253233", "kamil@interia.pl");
        clientService.savePerson(person);

        return person;
    }

    @GetMapping("/per")
    public List<Person> test2() {

        return clientService.getPeople();
    }

    @GetMapping("/service/orders")
    public List<Notification> getNotifications() {
        return clientService.getNotifications();
    }
}
