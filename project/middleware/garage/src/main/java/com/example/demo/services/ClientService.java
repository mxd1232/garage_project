package com.example.demo.services;


import com.example.demo.models.Notification;
import com.example.demo.models.Person;
import com.example.demo.repositories.NotificationRepository;
import com.example.demo.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService {

    @Autowired
    NotificationRepository notificationRepository;

    @Autowired
    PersonRepository personRepository;

    public void saveNotification(Notification notification){
        notificationRepository.save(notification);
    }

    public List<Notification> getNotifications(){
       return notificationRepository.findAll();
    }

    public Notification getNotificationById(String id){
        return notificationRepository.findById(id).get();
    }

    public void editNotification(Notification updatedNotification){
        notificationRepository.deleteById(updatedNotification.getId());
        notificationRepository.save(updatedNotification);
    }

    public void savePerson(Person person){
        personRepository.save(person);
    }

    public List<Person> getPeople(){
        return personRepository.findAll();
    }

    public Person getPersonById(String id){
        return personRepository.findById(id).get();
    }

}
