package com.example.demo.enums;

public enum StatusNotification {
    NEW,
    IN_PROGRESS,
    ENDED
}
